package com.example.appk02

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import kotlin.math.pow

class MainActivity : AppCompatActivity() {
    private lateinit var btnCalcular: Button
    private lateinit var btnLimpiar: Button
    private lateinit var btnSalir: Button
    private lateinit var txtPeso: EditText
    private lateinit var txtAltura: EditText
    private lateinit var lblImc : TextView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btnCalcular = findViewById(R.id.btnCalcular)
        btnSalir = findViewById(R.id.btnSalir)
        btnLimpiar = findViewById(R.id.btnLimpiar)
        txtPeso = findViewById(R.id.txtPeso)
        txtAltura = findViewById(R.id.txtAltura)
        lblImc = findViewById(R.id.lblImc)

        btnCalcular.setOnClickListener{
            if (txtAltura.text.toString().contentEquals("")){
                Toast.makeText(applicationContext,"Ingresa tu Altura",Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if (txtPeso.text.toString().contentEquals("")){
                Toast.makeText(applicationContext,"Ingresa tu Peso",Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            var altura : Double = txtAltura.text.toString().toDouble()
            var peso : Double = txtPeso.text.toString().toDouble()

            altura = altura.pow(2.0)
            val imc : Double = peso / altura
            val stringImc = "Tu IMC es de ${"%.2f".format(imc)}"

            lblImc.text = stringImc
        }
        btnLimpiar.setOnClickListener{
            txtAltura.text.clear()
            txtPeso.text.clear()
            lblImc.text=""
            txtAltura.requestFocus()
        }
        btnSalir.setOnClickListener{
            finish()
        }
    }
}